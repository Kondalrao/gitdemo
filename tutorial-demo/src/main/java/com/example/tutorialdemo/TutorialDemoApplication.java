package com.example.tutorialdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialDemoApplication {
    //Master changes updated here
	public static void main(String[] args) {
		SpringApplication.run(TutorialDemoApplication.class, args);
	}

}
