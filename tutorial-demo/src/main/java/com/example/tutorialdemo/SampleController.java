package com.example.tutorialdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/status")
    public String status() {
        return "Its UP in DEV, and its running fine!!";
    }
}
